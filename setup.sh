#!/bin/bash

# Pull changes
git pull

# Sync changes excluding .git directory
# rsync -qauh ./* "/etc/nginx" --exclude=".git"

# Set proper permissions
chmod -R 644 /etc/nginx
find /etc/nginx -type d -exec chmod 700 {} \;

# If you store SSL certs under `/etc/nginx/ssl`
# Set proper permission for SSL certs 
# chmod -R 600 /etc/nginx/ssl
# chmod -R 400 /etc/nginx/ssl/*

# Reload nginx config
# but only if configtest is passed
nginx -t && service nginx reload
